class Funcionarios{
    constructor(
        private _nome:string, 
        private _salarioHora: number, 
        private _diasTrabalhados: number, 
        private _faltasAnual: number, 
    ){}

    get nome(){
        return this._nome;
    }

    get salarioHora(){
        return this._salarioHora;
    }

    get diasTrabalhados(){
        return this._diasTrabalhados;
    }

    get faltasAnual(){
        return this._faltasAnual;
    }

    set nome(nome: string){
        this._nome = nome;
    }

    set salarioHora(salarioHora: number){
        this._salarioHora = salarioHora;
    }

    set diasTrabalhados(diasTrabalhados: number){
        this._diasTrabalhados = diasTrabalhados;
    }

    set faltasAnual(faltasAnual: number){
        this._faltasAnual = faltasAnual;
    }

    salarioBruto(){
        return this.salarioHora * this.diasTrabalhados
    }

    plr(){
        if(this.faltasAnual == 0){
            return this.salarioBruto()
        } else if (this.faltasAnual == 1){
            return this.salarioBruto() * 0.94
        } else if (this.faltasAnual == 2){
            return this.salarioBruto() * 0.92
        } else if (this.faltasAnual == 3){
            return this.salarioBruto() * 0.90
        } else if (this.faltasAnual == 4){
            return this.salarioBruto() * 0.88
        } else {
            return this.salarioBruto() * 0
        }
    }

    descontoSalario(){
        return this.salarioBruto() * 0.05
    }

    salarioLiq(){
        return this.salarioBruto() - this.descontoSalario() + this.plr()
    }
}

const funcionarios = new Funcionarios('Abner', 100, 30, 0)
console.log(funcionarios)

console.log(`\n\n O funcionario de nome ${funcionarios.nome} tem o salário bruto de
${funcionarios.salarioBruto()}, teve ${funcionarios.faltasAnual} falta(s) e sua
PLR foi de ${funcionarios.plr()} \n\n`)

console.log(`O funcionario de nome ${funcionarios.nome} tem o salário bruto de
${funcionarios.salarioBruto()}, o desconto de ${funcionarios.descontoSalario()}, 
a sua PLR de ${funcionarios.plr()} e o salário liquido de ${funcionarios.salarioLiq()}`)