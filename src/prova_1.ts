class Motos {
    constructor(
        private _produto: string,
        private _preco: number,
        private _quantidade: number
    ){}

    get produto() {
        return this._produto;
    }

    get preco(){
        return this._preco;
    }

    get quantidade() {
        return this._quantidade;
    }

    set produto(produto: string){
        if(produto == ''){
            throw new Error("Produto vazio")
        }
        this._produto = produto
    }

    set preco(preco: number){
        if(preco == 0){
            throw new Error("Preco invalido")
        }
        this._preco = preco
    }

    set quantidade(quantidade: number){
        if(quantidade == 0){
            throw new Error("Quantidade invalida")
        }
        this._quantidade = quantidade
    }   

    desconto(){
        if(this.quantidade <= 10){
            return 0
        } else if (this.quantidade >= 11 && this.quantidade <= 20){
            return this.preco * 0.10
        } else if (this.quantidade >= 21 && this.quantidade <= 50){
            return this.preco * 0.20
        } else {
            return this.preco * 0.25
        }
    }

    calculoValor(){
        return this.preco - this.desconto()
    }
}

const motos = new Motos('MT09', 10000, 1);
console.log(motos);

// try {
//     motos.preco = 1000
//     motos.quantidade = 2
//     console.log(`O produto ${motos.produto} de preço ${motos.preco}, quantidade comprada 
//     de ${motos.quantidade} teve o desconto de ${motos.desconto()} e o valor pago foi de ${motos.calculoValor() * motos.quantidade}`)
// } catch (err) {
//     console.log(err.message)
// }
