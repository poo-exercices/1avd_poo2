class ConversaoUnidades{

    static minToSecond(minuto: number){
        return `${minuto * 60} segundo(s)`
    }

    static hourToMin(hora: number){
        return `${hora * 60} minuto(s)`
    }

    static dayToHour(dia: number){
        return `${dia * 24} hora(s)`
    }

    static weekToDay(semana: number){
        return `${semana * 7} dia(s)`
    }

    static MonthToDay(mes: number){
        return `${mes * 30} dias(s)`
    }

    static yearToDay(ano: number){
        return `${ano * 365.25} dias`
    }
}

// console.log(ConversaoUnidades.minToSecond(1))
// console.log(ConversaoUnidades.hourToMin(1))
// console.log(ConversaoUnidades.dayToHour(1))
// console.log(ConversaoUnidades.weekToDay(1))
// console.log(ConversaoUnidades.MonthToDay(1))
// console.log(ConversaoUnidades.yearToDay(1))