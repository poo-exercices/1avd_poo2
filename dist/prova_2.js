"use strict";
var ConversaoUnidades = /** @class */ (function () {
    function ConversaoUnidades() {
    }
    ConversaoUnidades.minToSecond = function (minuto) {
        return minuto * 60 + " segundo(s)";
    };
    ConversaoUnidades.hourToMin = function (hora) {
        return hora * 60 + " minuto(s)";
    };
    ConversaoUnidades.dayToHour = function (dia) {
        return dia * 24 + " hora(s)";
    };
    ConversaoUnidades.weekToDay = function (semana) {
        return semana * 7 + " dia(s)";
    };
    ConversaoUnidades.MonthToDay = function (mes) {
        return mes * 30 + " dias(s)";
    };
    ConversaoUnidades.yearToDay = function (ano) {
        return ano * 365.25 + " dias";
    };
    return ConversaoUnidades;
}());
// console.log(ConversaoUnidades.minToSecond(1))
// console.log(ConversaoUnidades.hourToMin(1))
// console.log(ConversaoUnidades.dayToHour(1))
// console.log(ConversaoUnidades.weekToDay(1))
// console.log(ConversaoUnidades.MonthToDay(1))
// console.log(ConversaoUnidades.yearToDay(1))
