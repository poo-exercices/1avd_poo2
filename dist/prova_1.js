"use strict";
var Motos = /** @class */ (function () {
    function Motos(_produto, _preco, _quantidade) {
        this._produto = _produto;
        this._preco = _preco;
        this._quantidade = _quantidade;
    }
    Object.defineProperty(Motos.prototype, "produto", {
        get: function () {
            return this._produto;
        },
        set: function (produto) {
            if (produto == '') {
                throw new Error("Produto vazio");
            }
            this._produto = produto;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Motos.prototype, "preco", {
        get: function () {
            return this._preco;
        },
        set: function (preco) {
            if (preco == 0) {
                throw new Error("Preco invalido");
            }
            this._preco = preco;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Motos.prototype, "quantidade", {
        get: function () {
            return this._quantidade;
        },
        set: function (quantidade) {
            if (quantidade == 0) {
                throw new Error("Quantidade invalida");
            }
            this._quantidade = quantidade;
        },
        enumerable: false,
        configurable: true
    });
    Motos.prototype.desconto = function () {
        if (this.quantidade <= 10) {
            return 0;
        }
        else if (this.quantidade >= 11 && this.quantidade <= 20) {
            return this.preco * 0.10;
        }
        else if (this.quantidade >= 21 && this.quantidade <= 50) {
            return this.preco * 0.20;
        }
        else {
            return this.preco * 0.25;
        }
    };
    Motos.prototype.calculoValor = function () {
        return this.preco - this.desconto();
    };
    return Motos;
}());
var motos = new Motos('MT09', 10000, 1);
console.log(motos);
// try {
//     motos.preco = 1000
//     motos.quantidade = 2
//     console.log(`O produto ${motos.produto} de preço ${motos.preco}, quantidade comprada 
//     de ${motos.quantidade} teve o desconto de ${motos.desconto()} e o valor pago foi de ${motos.calculoValor()}`)
// } catch (err) {
//     console.log(err.message)
// }
