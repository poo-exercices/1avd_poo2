"use strict";
var Funcionarios = /** @class */ (function () {
    function Funcionarios(_nome, _salarioHora, _diasTrabalhados, _faltasAnual) {
        this._nome = _nome;
        this._salarioHora = _salarioHora;
        this._diasTrabalhados = _diasTrabalhados;
        this._faltasAnual = _faltasAnual;
    }
    Object.defineProperty(Funcionarios.prototype, "nome", {
        get: function () {
            return this._nome;
        },
        set: function (nome) {
            this._nome = nome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Funcionarios.prototype, "salarioHora", {
        get: function () {
            return this._salarioHora;
        },
        set: function (salarioHora) {
            this._salarioHora = salarioHora;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Funcionarios.prototype, "diasTrabalhados", {
        get: function () {
            return this._diasTrabalhados;
        },
        set: function (diasTrabalhados) {
            this._diasTrabalhados = diasTrabalhados;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Funcionarios.prototype, "faltasAnual", {
        get: function () {
            return this._faltasAnual;
        },
        set: function (faltasAnual) {
            this._faltasAnual = faltasAnual;
        },
        enumerable: false,
        configurable: true
    });
    Funcionarios.prototype.salarioBruto = function () {
        return this.salarioHora * this.diasTrabalhados;
    };
    Funcionarios.prototype.plr = function () {
        if (this.faltasAnual == 0) {
            return this.salarioBruto();
        }
        else if (this.faltasAnual == 1) {
            return this.salarioBruto() * 0.94;
        }
        else if (this.faltasAnual == 2) {
            return this.salarioBruto() * 0.92;
        }
        else if (this.faltasAnual == 3) {
            return this.salarioBruto() * 0.90;
        }
        else if (this.faltasAnual == 4) {
            return this.salarioBruto() * 0.88;
        }
        else {
            return this.salarioBruto() * 0;
        }
    };
    Funcionarios.prototype.descontoSalario = function () {
        return this.salarioBruto() * 0.05;
    };
    Funcionarios.prototype.salarioLiq = function () {
        return this.salarioBruto() - this.descontoSalario() + this.plr();
    };
    return Funcionarios;
}());
var funcionarios = new Funcionarios('Abner', 100, 30, 0);
console.log(funcionarios);
console.log("\n\n O funcionario de nome " + funcionarios.nome + " tem o sal\u00E1rio bruto de\n" + funcionarios.salarioBruto() + ", teve " + funcionarios.faltasAnual + " falta(s) e sua\nPLR foi de " + funcionarios.plr() + " \n\n");
console.log("O funcionario de nome " + funcionarios.nome + " tem o sal\u00E1rio bruto de\n" + funcionarios.salarioBruto() + ", o desconto de " + funcionarios.descontoSalario() + ", \na sua PLR de " + funcionarios.plr() + " e o sal\u00E1rio liquido de " + funcionarios.salarioLiq());
